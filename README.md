# LEMP configuration for Vagrant #

Automated configuration files for a LEMP development environment using Vagrant.

### Requirements ###

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)


### Configuration ###

* Ubuntu 16.04
* Nginx
* Mysql
* PHP 7.1

### How do I get set up? ###

git clone https://bitbucket.org/activelab/vagrant-lemp

cd vagrant-lemp

vagrant up


### Notes ###

* Mysql credentials root/root
