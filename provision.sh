#!/usr/bin/env bash
#
# Provision for:
# LEMP Ubuntu 16.04, Nginx, MySQL and PHP7.1
#
# Codesynthesis, David Bainbridge
#

# Turn off front end prompts during installation.
export DEBIAN_FRONTEND=noninteractive

# Map /var/www to shared /vagrant/sites directory
if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant /var/www
fi

# Update the package lists for upgrades
apt-get update

# Install nginx -y for auto-yes
apt-get install -y nginx

# Install MySQL, prepopulate password to 'root'
echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
apt-get install -y mysql-server mysql-client

# Update the repositories for PHP7 (not required after Ubuntu 16.10)
add-apt-repository ppa:ondrej/php
apt-get update

# Install PHP and MySQL helper packagemysql 
apt-get install -yq php7.1-fpm php7.1-mysql

# Configure the host file
cat >> /etc/nginx/sites-available/default <<'EOF'
server {
	# Server listening port
	listen 80;

	# Server domain or IP
	server_name localhost;

	# Root and index files
	root /vagrant/default/public;
	index index.php index.html index.htm;	

	# Urls to attempt
	location / {
                try_files $uri $uri/ /index.php?$query_string;
        }

	# Configure PHP FPM
	location ~* \.php$ {
		fastcgi_pass unix:/run/php/php7.1-fpm.sock;
		fastcgi_index index.php;
		fastcgi_split_path_info ^(.+\.php)(.*)$;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		include /etc/nginx/fastcgi_params;
	}

	# Debugging
	access_log /var/log/nginx/localhost_access.log;
	error_log /var/log/nginx/localhost_error.log;
	rewrite_log on;
}
EOF

# Restart
service nginx restart
service php7.1-fpm restart

